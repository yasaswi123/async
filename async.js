const fs = require('node:fs').promises
const path = require('node:path')

async function operations() {
    try {
        const data = await fs.readFile('./index.json', 'utf-8');
        const converting = JSON.parse(data);
        let answer = {};
        answer['name'] = converting.name;
        const array=[]
        for (const topics of converting['topics']) {
            const topic = {};
            const directory = topics.directory_path;
            const fileName = path.join(directory, './index.json');
            const data1 = await fs.readFile(fileName, 'utf-8');
            const parts = JSON.parse(data1).parts;
            topic['id'] = topics.id;
            topic['name'] = JSON.parse(data1).name;
            topic['directory_path'] = topics.directory_path;
            topic['parts'] = [];

            for (const filelocation of parts) {
                const part = {};
                const location = filelocation.file_location;
                const data2 = await fs.readFile(location, 'utf-8');
                part['name'] = filelocation.name;
                part['content'] = data2;
                topic['parts'].push(part);
            }
            array.push(topic)
        }
        answer['Topics']=array
        console.log(answer)
        await fs.writeFile('./output.json',JSON.stringify(answer),'utf-8')
    } 
    catch (error) {
        console.error(error);
    }
}

operations();


